import java.util.Scanner;

public class Time
{
    public static void travelTime() {
        Scanner input = new Scanner(System.in);
        // Part 2 - Travel Time
        System.out.print("\nHow many days are they going to spend at your destination? ");
        int daysOfTrip = input.nextInt();
        int hoursOfTrip = daysOfTrip * 24;
        System.out.print("If you are traveling to " + TravelProject.destination + " for " + daysOfTrip + " days, that is the same as " + hoursOfTrip + " hours.");
        // Part 2 - Budget
        System.out.print("\nWhat is your total budget for the trip in USD? $");

        int tripBudgetUsd = input.nextInt();
        int tripBudgetKrw = tripBudgetUsd * 1000;
        int tripBudgetJpy = tripBudgetUsd * 100;
        int usBudgetPerDay = tripBudgetUsd / daysOfTrip;
        int jpBudgetPerDay = tripBudgetJpy / daysOfTrip;
        int krBudgetPerDay = tripBudgetKrw / daysOfTrip;

        if (TravelProject.korea) {
            System.out.println("Your budget is $" + tripBudgetUsd + ". The exchange rate is ₩" + tripBudgetKrw + ".");
            System.out.println("Your budget is ₩" + tripBudgetKrw + ". You can spend ₩" + krBudgetPerDay + " on a daily basis.");
        } else if (TravelProject.japan) {
            System.out.println("Your budget is $" + tripBudgetUsd + ". The exchange rate is ¥" + tripBudgetJpy + ".");
            System.out.println("Your budget is ¥" + tripBudgetJpy + ". You can spend ¥" + jpBudgetPerDay + " on a daily basis.");
        } else {
            System.out.println("Your budget is $" + tripBudgetUsd + ".");
            System.out.println("Your budget is $" + tripBudgetUsd + ". You can spend $" + usBudgetPerDay + " on a daily basis.");
        }
    }

    public static void timeDifference() {
        Scanner input = new Scanner(System.in);
        System.out.print("What is the time zone difference of your home to your destination? ");
        int timeDifference = input.nextInt();
        int timeDifferenceFromDestination = timeDifference % 24;
        System.out.println("The time will be " + timeDifferenceFromDestination + ":00, when it is midnight local time.");
    }
}
