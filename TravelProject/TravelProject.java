public class TravelProject {

    public static String destination;
    public static boolean japan;
    public static boolean korea;
    public static boolean usa;

    public static void main(String[] args){
            // Part 1 - Greeting
        Greeting.greeting();
        Destination.travelDestination();
            // Part 2 - Travel Time and Budget
        Time.travelTime();
            // Part 3 - Time Difference
        Time.timeDifference();
            // Part 4 - Country Area
        Destination.areaOfDestination();
    }
}
