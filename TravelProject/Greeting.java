import java.util.Scanner;

public class Greeting
{
    public static void greeting() {
        // Part 1 - Greeting
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to your VACATION PLANNER!");
        System.out.print("Who will be going on this trip? ");
        String names = input.nextLine();
        System.out.println(names.toUpperCase() + " let's get started!");
        System.out.print("Where you travelling to? ");
        TravelProject.destination = input.next();
    }
}
