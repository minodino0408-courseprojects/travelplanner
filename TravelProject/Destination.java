import java.util.Scanner;

public class Destination
{
    public static void travelDestination() {
        Scanner input = new Scanner(System.in);

        if (TravelProject.japan = TravelProject.destination.contains("Japan") || TravelProject.destination.contains("Tokyo") || TravelProject.destination.contains("Osaka") || TravelProject.destination.contains("Kyoto")){
            // Traveling to Japan
            System.out.println("Great! " + TravelProject.destination.toUpperCase() + " sounds like a great trip. There are lots to do in the Land of the Rising Sun");
        } else if (TravelProject.korea = TravelProject.destination.contains("Korea") || TravelProject.destination.contains("Seoul") || TravelProject.destination.contains("Busan")){
            //Traveling to Korea
            System.out.print("Great! " + TravelProject.destination.toUpperCase() + " sounds like a great trip. There are lots to do in the Land of the Morning Calm");
        } else if (TravelProject.usa = TravelProject.destination.contains("US") || TravelProject.destination.contains("USA") || TravelProject.destination.contains("NYC") || TravelProject.destination.contains("LA") || TravelProject.destination.contains("Seattle") || TravelProject.destination.contains("Chicago")){
            // Traveling within the US
            System.out.print("Great! " + TravelProject.destination.toUpperCase() + " sounds like a great trip. There are lots to do in America.");
        } else{
            System.out.print("Destination not recognized. Please try again later");
            System.exit(0);
        }
    }

    public static void areaOfDestination() {
        Scanner input = new Scanner(System.in);
        System.out.print("What is the area of your destination in km^2? ");
        double areaOfDestinationInKm = input.nextDouble();
        int  areaMath = (int) (areaOfDestinationInKm * 0.62137);
        double areaOfDestinationInMiles = areaMath / 100.0;
        System.out.println("The area of " + TravelProject.destination + " in miles is " + areaOfDestinationInMiles + ".");
    }
}
